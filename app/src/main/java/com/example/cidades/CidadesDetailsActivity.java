package com.example.cidades;


/**
 * Created by csalmeida on 12/08/2017.
 */

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;

public class CidadesDetailsActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {

            finish();
            return;
        }

        if (savedInstanceState == null) {

            CidadesDetailsFragment details = new CidadesDetailsFragment();
            details.setArguments(getIntent().getExtras());
            getFragmentManager().beginTransaction().add(android.R.id.content, details).commit();
        }
    }


}
