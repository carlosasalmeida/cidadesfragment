package com.example.cidades;


import android.app.Fragment;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ScrollView;

/**
 * Created by csalmeida on 12/08/2017.
 */

public class CidadesDetailsFragment extends Fragment{

    // Cria uma nova instancia de detailsfRagment
    public static CidadesDetailsFragment newInstance(int index) {
        CidadesDetailsFragment cdf = new CidadesDetailsFragment();
        Bundle args = new Bundle();
        args.putInt("index", index);
        cdf.setArguments(args);
        return cdf;
    }
    public int getShowIndex() {
        return getArguments().getInt("index",0);
    }
    // Devolve um objecto ScrolView com um WebView carregado com a pagina Web
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        final String[] links = getResources().getStringArray(R.array.pages);
        ScrollView scroller = new ScrollView(getActivity());
        WebView webview = new WebView(getActivity());
        webview.setWebViewClient(new  SwAWebClient());
        int padding =
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,4,getActivity().getResources().getDisplayMetrics());
        webview.setPadding(padding,padding,padding,padding);
        scroller.addView(webview);
        webview.loadUrl(links[getShowIndex()]);
        return scroller;
    }
    private class SwAWebClient extends WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }
    }
}
