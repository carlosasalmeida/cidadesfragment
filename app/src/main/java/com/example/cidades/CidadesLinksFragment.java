package com.example.cidades;

import android.content.Intent;
import android.os.Bundle;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;





/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class CidadesLinksFragment extends ListFragment {
    boolean dualFrame;
    int curPosition = 0;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Cria a lista com o array definido no ficheiro XML
        setListAdapter(ArrayAdapter.createFromResource(getActivity(),R.array.cidades,android.R.layout.simple_list_item_activated_1));
        //Verifica a existência de frame para a página de detalhes
        View detailsFrame = getActivity().findViewById(R.id.details);
        dualFrame = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;

        if (savedInstanceState != null) {
            //Recupera o último estado para a posição marcada
            curPosition = savedInstanceState.getInt("curPosition", 0);
        }
        // se modo horizontal
        if ( dualFrame) {
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            showDetails(curPosition);
        }
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("curPosition",curPosition);
    }
	@Override
    public void onListItemClick(ListView l, View v,int position , long id) {
        showDetails(position);
    }
    // Mostra os detalhes do item seleccionado quer apresentando o fragmento
    // do lado direito do ecrã ou iniciando uma nova atividade
    private void showDetails(int index) {
        curPosition = index;
        if (dualFrame) {
            getListView().setItemChecked(index, true);
            //Verifica que fragmento está exibido e altera-o se necessário
            CidadesDetailsFragment details = (CidadesDetailsFragment) getFragmentManager().findFragmentById(R.id.details);
            if (details == null || details.getShowIndex() != index) {                // Cria novo fragmento
                details = CidadesDetailsFragment.newInstance(index);                // Executa uma transação, substituindo qualquer outro fragmento
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.details, details);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
            }
        } else {
            // lançamento da nova actividade
            Intent intent = new Intent();
            intent.setClass(getActivity(), CidadesDetailsActivity.class);
            intent.putExtra("index", index);
            startActivity(intent);
        }
    }
}
